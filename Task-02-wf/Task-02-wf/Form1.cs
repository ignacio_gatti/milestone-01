﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_02_wf
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }



        //********************************************Program LOGIC**************************************************///
        static public double shoppingCart = 0;

        static double numberProcess(double userInput)/*This is where the user input is processed to include GST in it*/
        {

            double userInputProcess = userInput;
            double GST = 1.15;
            double numberReturnProcess = 0;

            numberReturnProcess = userInputProcess * GST;

            return numberReturnProcess;
        }
        //*****************End of numberProcess*************

        static string shoppingCartProcess(string userInputProcess)/*This is where the items get added together to show the total*/
        {
            double userInput = 0.0;
            userInput = double.Parse(userInputProcess);
            double numberReturn = 0;
            string showShoppingCart = "";

            numberReturn = numberProcess(userInput);

            shoppingCart += numberReturn;

            showShoppingCart = $"You Have ${shoppingCart} in your shopping cart including GST. ";

            return showShoppingCart;
        }//****************End of shoppingCartProcess*******************

        private void lblResult_Click(object sender, EventArgs e)
        {
            
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            
            lblResult.Text = $"{shoppingCartProcess(txtUserInput.Text)}";
            
        }

        private void btnCheckOut_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Thank You For Shopping With Us");
            System.Environment.Exit(1);
        }
    }
}
