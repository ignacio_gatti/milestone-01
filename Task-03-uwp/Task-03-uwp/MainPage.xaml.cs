﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task_03_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void btn1stCheck_Click(object sender, RoutedEventArgs e)
        {
            lbl2ndQ.Text = "What is your favourite: " + txtFav1.Text;
        }

        
        
        private void btn2ndCheck_Click(object sender, RoutedEventArgs e)
        {
            

            if (txtFav1.Text == "")
            {
                txtResult.Text = "Please Fill In The First Block";
            }

            else
            {
                userInput = txtFav1.Text;
                txtResult.Text = "Your Favourite " + txtFav1.Text + " is " + txtFav2.Text + ". " + optionsProcess(txtFav2.Text);
                
            }
        }

        /************************PROGRAM LOGIC**************************************/

        static public string userInput = "";

        static string optionsProcess(string userInputSent)
        {
            string userInputProcess = userInputSent;
            string dictResult = "";
            var responsesPC = new Dictionary<string, string>();

            /*Favourites*/
            responsesPC.Add("Red", $"That is my favourite {userInput} too!");
            responsesPC.Add("Diamond", $"That is my favourite {userInput} too!");
            responsesPC.Add("Summer", $"That is my favourite {userInput} too!");
            /*Least Favourites*/
            responsesPC.Add("Green", $"Really? That is my least favourite {userInput}.");
            responsesPC.Add("Spade", $"Really? That is my least favourite {userInput}.");
            responsesPC.Add("Winter", $"Really? That is my least favourite {userInput}.");
            /*Good*/
            responsesPC.Add("Blue", $"Not my favourite {userInput}, but {userInputProcess} is still really good.");
            responsesPC.Add("Heart", $"Not my favourite {userInput}, but {userInputProcess} is still really good.");
            responsesPC.Add("Club", $"Not my favourite {userInput}, but {userInputProcess} is still really good.");
            responsesPC.Add("Autumn", $"Not my favourite {userInput}, but {userInputProcess} is still really good.");
            responsesPC.Add("Spring", $"Not my favourite {userInput}, but {userInputProcess} is still really good.");

            switch (userInputProcess)
            {
                case "red":
                    dictResult = responsesPC["Red"];
                    break;

                case "diamond":
                    dictResult = responsesPC["Diamond"];
                    break;


                case "summer":
                    dictResult = responsesPC["Summer"];
                    break;

                case "green":
                    dictResult = responsesPC["Green"];
                    break;

                case "spade":
                    dictResult = responsesPC["Spade"];
                    break;

                case "winter":
                    dictResult = responsesPC["Winter"];
                    break;

                case "blue":
                    dictResult = responsesPC["Blue"];
                    break;

                case "club":
                    dictResult = responsesPC["Club"];
                    break;

                case "autumn":
                    dictResult = responsesPC["Autumn"];
                    break;

                case "spring":
                    dictResult = responsesPC["Spring"];
                    break;

                case "heart":
                    dictResult = responsesPC["Heart"];
                    break;

                default:
                    dictResult = "That is not an option. Please Choose A Valid Option";
                    break;
            }

            return dictResult;


        }/*************END OF optionsProcess**************************/
    }
}
