﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_04_console
{
    class Program
    {
        static void Main(string[] args)
        {
            string userInput = "";
            string FruAndVegReturn = "";
            
            do
            {
            Restart:
                Console.WriteLine($"Type in a Fruit or Vegetable");
                userInput = Console.ReadLine();

                string userInputLowerCase = userInput.ToLower();

                FruAndVegReturn = FruAndVegProcess(userInputLowerCase);

                if (FruAndVegReturn == "quit")
                {
                    break;
                }

                else if (FruAndVegReturn == "\nThis Fruit/Vegetable Is Not In The List\n")
                {
                    Console.WriteLine(FruAndVegReturn);
                    goto Restart;
                }

                Console.WriteLine($"This {userInputLowerCase} is {FruAndVegReturn}");
                Console.ReadKey();
            } while (FruAndVegReturn != "quit");
            

        }/*END OF MAIN METHOD*/

        /********************************PROGRAM LOGIC*****************************/

        static string FruAndVegProcess(string userInput)
        {
            string userInputProcess = userInput;
            string FruAndVegReturn = "";
            var fruitAndVeg = new Dictionary<string, string>();

            fruitAndVeg.Add("banana", "yellow");
            fruitAndVeg.Add("apple", "red");
            fruitAndVeg.Add("orange", "orange");
            fruitAndVeg.Add("lettuce", "green");
            fruitAndVeg.Add("tomatoe", "red");
            fruitAndVeg.Add("garlic", "white");
            fruitAndVeg.Add("cherry", "purple");
            fruitAndVeg.Add("carrot" , "orange");

            
            switch (userInputProcess)
            {
                case "banana":
                    FruAndVegReturn = fruitAndVeg["banana"];
                    break;

                case "apple":
                    FruAndVegReturn = fruitAndVeg["apple"];
                    break;

                case "orange":
                    FruAndVegReturn = fruitAndVeg["orange"];
                    break;

                case "lettuce":
                    FruAndVegReturn = fruitAndVeg["lettuce"];
                    break;

                case "tomatoe":
                    FruAndVegReturn = fruitAndVeg["tomatoe"];
                    break;

                case "garlic":
                    FruAndVegReturn = fruitAndVeg["garlic"];
                    break;

                case "cherry":
                    FruAndVegReturn = fruitAndVeg["cherry"];
                    break;

                case "carrot":
                    FruAndVegReturn = fruitAndVeg["carrot"];
                    break;

                case "quit":
                    FruAndVegReturn = "quit";
                    break;



                default:
                    FruAndVegReturn = "\nThis Fruit Is Not In The List\n";
                    break;
            }

            
            return FruAndVegReturn;
        }/*********************END OF FruAndVegProcess*************************/
    }
}
