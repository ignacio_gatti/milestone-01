﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_02_console
{
    class Program
    {
        static public double shoppingCart = 0;

        static void Main(string[] args)
        {


            string choice = "";
            string choiceLowerCase = "";
            var shoppingCartReturn = "";
            string userInput = "";


        Restart:
            Console.WriteLine($"\nWrite The Cost of Item");
            userInput = Console.ReadLine();

            double userInputTurnedToDouble = 0.0;
            bool userInputNotDouble = double.TryParse(userInput, out userInputTurnedToDouble);/*checks to see if userInput is a number*/


            if (!userInputNotDouble)/*if the userInput is not a number, an error message pops up and lets the user start again*/
            {
                Console.WriteLine("This is not a number. Please Enter a Number");
                goto Restart;
            }

            shoppingCartReturn = shoppingCartProcess(userInput);

            Console.Write($"{shoppingCartReturn}\n\nWould You Like To Continue?\nPress <ENTER> To Continue\nType \"quit\" To Chekout   ");
            choice = Console.ReadLine();

            choiceLowerCase = choice.ToLower(); /*This is used to turn the user's choices to lower case*/

            switch (choiceLowerCase)
            {
                case "quit":
                    break;


                default:
                    goto Restart;
            }

            Console.WriteLine($"Thank You For Shopping With Us. Your Total is ${shoppingCart}.\nPlease Vist Us Soon");
            Console.ReadLine();
            System.Environment.Exit(1);


        }
        //***********************************End of Main Function****************************************************



        //********************************************Program LOGIC**************************************************///


        static double numberProcess(double userInput)/*This is where the user input is processed to include GST in it*/
        {

            double userInputProcess = userInput;
            double GST = 1.15;
            double numberReturnProcess = 0;

            numberReturnProcess = userInputProcess * GST;

            return numberReturnProcess;
        }
        //*****************End of numberProcess*************

        static string shoppingCartProcess(string userInputProcess)/*This is where the items get added together to show the total*/
        {
            double userInput = 0.0;
            userInput = double.Parse(userInputProcess) ;
            double numberReturn = 0;
            string showShoppingCart = "";

            numberReturn = numberProcess(userInput);

            shoppingCart += numberReturn;

            showShoppingCart = $"You Have ${shoppingCart} in your shopping cart including GST. ";

            return showShoppingCart;
        }
        //****************End of shoppingCartProcess*******************

    }


}
