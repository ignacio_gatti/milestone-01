﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task_01_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void btnCon2Km_Click(object sender, RoutedEventArgs e)
        {
            
            txtResult.Text = $"{convertToKmProcess(txtUserInput.Text)}" + " Km";
        }

        private void btnCon2Miles_Click(object sender, RoutedEventArgs e)
        {
            txtResult.Text = $"{convertToMilesProcess(txtUserInput.Text)}" + " Miles";
        }

        /*************************PROGRAM LOGIC******************/
        static double convertToMilesProcess(string txtInput)
        {
            const double km2Miles = 0.621371192;
            double km = double.Parse(txtInput);
            var answer = km * km2Miles;
            answer = System.Math.Round(answer, 2);
            return answer;
        }



        static double convertToKmProcess(string input)
        {
            const double miles2Km = 1.609344;
            double miles = double.Parse(input);
            var answer = miles * miles2Km;
            answer = System.Math.Round(answer, 2);
            return answer;
        }
    }
}
