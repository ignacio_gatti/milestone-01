﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_05_Test_Dummy
{
    class Program
    {
        static void Main(string[] args)
        {
            int userInput = 1;
            string ResultsReturn;
            string endOfGameChoice = "";
            string endOfGameChoiceLowerCase = "";
        Restart:
            //Console.Clear();
            Console.WriteLine("Please Choose A Number Between 1 and 5");
            userInput = int.Parse(Console.ReadLine());

            ResultsReturn = gameProcess(userInput);

            Console.WriteLine(ResultsReturn);

            if (livesLeft > 0)
            {
                
                goto Restart;
            }

            else
            {
                Console.WriteLine("You Lose! Sorry! \nPress <ENTER> To Start Again | Type \"Quit\" To Leave");
                endOfGameChoice = Console.ReadLine();
                endOfGameChoiceLowerCase = endOfGameChoice.ToLower();

                if (endOfGameChoiceLowerCase == "quit")
                {
                    Console.WriteLine("\nSorry To See You Go. Come Back Soon!");
                    System.Environment.Exit(1);
                }

                else
                {
                    livesLeft += 5;
                    Console.Clear();
                    Console.WriteLine($"Your Winning Streak Was {winningStreak}. Think You Can Do Better?");
                    Console.ReadKey();
                    Console.Clear();
                    winningStreak = 0;
                    goto Restart;
                }
            }

        }

        /*******************************PROGRAM LOGIC***************************/

        static public int livesLeft = 5;
        static public int winningStreak = 0;

        static int RanProcess(int input)
        {
            Random rnd = new Random();
            int ranNumber = rnd.Next(1, 6);
            return ranNumber;
        }/*******************END OF RanProcess******************/

        static string gameProcess(int userInputSend)
        {
            int userInputProcess = userInputSend;
            int ranNumberProcess = RanProcess(userInputProcess);
            string resultReturn = "";


            if (userInputProcess == ranNumberProcess)
            {
                resultReturn = $"You Chose {userInputProcess}  The PC chose {ranNumberProcess}. You Got It Right! \nYou Have {livesLeft} Lives Left\n";
                winningStreak++;
            }
            else if (userInputProcess != ranNumberProcess)
            {
                livesLeft--;
                resultReturn = $"You Chose {userInputProcess}  The PC chose {ranNumberProcess}. You Lose 1 Life \nYou Have {livesLeft} Lives Left\n";
                
            }

            return resultReturn;

        }/******************END OF gameProcess********************/


    }
}
