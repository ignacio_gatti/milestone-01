﻿namespace Task_05_wf
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnNo1 = new System.Windows.Forms.Button();
            this.btnNo2 = new System.Windows.Forms.Button();
            this.btnNo3 = new System.Windows.Forms.Button();
            this.btnNo4 = new System.Windows.Forms.Button();
            this.btnNo5 = new System.Windows.Forms.Button();
            this.lblLivesLeft = new System.Windows.Forms.Label();
            this.btnRestart = new System.Windows.Forms.Button();
            this.lblWinningStreak = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnNo1
            // 
            this.btnNo1.Location = new System.Drawing.Point(49, 55);
            this.btnNo1.Name = "btnNo1";
            this.btnNo1.Size = new System.Drawing.Size(108, 55);
            this.btnNo1.TabIndex = 0;
            this.btnNo1.Text = "1";
            this.btnNo1.UseVisualStyleBackColor = true;
            this.btnNo1.Click += new System.EventHandler(this.btnNo1_Click);
            // 
            // btnNo2
            // 
            this.btnNo2.Location = new System.Drawing.Point(184, 55);
            this.btnNo2.Name = "btnNo2";
            this.btnNo2.Size = new System.Drawing.Size(109, 55);
            this.btnNo2.TabIndex = 1;
            this.btnNo2.Text = "2";
            this.btnNo2.UseVisualStyleBackColor = true;
            this.btnNo2.Click += new System.EventHandler(this.btnNo2_Click);
            // 
            // btnNo3
            // 
            this.btnNo3.Location = new System.Drawing.Point(49, 134);
            this.btnNo3.Name = "btnNo3";
            this.btnNo3.Size = new System.Drawing.Size(108, 55);
            this.btnNo3.TabIndex = 2;
            this.btnNo3.Text = "3";
            this.btnNo3.UseVisualStyleBackColor = true;
            this.btnNo3.Click += new System.EventHandler(this.btnNo3_Click);
            // 
            // btnNo4
            // 
            this.btnNo4.Location = new System.Drawing.Point(184, 134);
            this.btnNo4.Name = "btnNo4";
            this.btnNo4.Size = new System.Drawing.Size(109, 55);
            this.btnNo4.TabIndex = 3;
            this.btnNo4.Text = "4";
            this.btnNo4.UseVisualStyleBackColor = true;
            this.btnNo4.Click += new System.EventHandler(this.btnNo4_Click);
            // 
            // btnNo5
            // 
            this.btnNo5.Location = new System.Drawing.Point(117, 208);
            this.btnNo5.Name = "btnNo5";
            this.btnNo5.Size = new System.Drawing.Size(108, 54);
            this.btnNo5.TabIndex = 4;
            this.btnNo5.Text = "5";
            this.btnNo5.UseVisualStyleBackColor = true;
            this.btnNo5.Click += new System.EventHandler(this.btnNo5_Click);
            // 
            // lblLivesLeft
            // 
            this.lblLivesLeft.AutoSize = true;
            this.lblLivesLeft.Location = new System.Drawing.Point(49, 13);
            this.lblLivesLeft.Name = "lblLivesLeft";
            this.lblLivesLeft.Size = new System.Drawing.Size(113, 13);
            this.lblLivesLeft.TabIndex = 5;
            this.lblLivesLeft.Text = "You Have 5 Lives Left";
            // 
            // btnRestart
            // 
            this.btnRestart.Location = new System.Drawing.Point(49, 300);
            this.btnRestart.Name = "btnRestart";
            this.btnRestart.Size = new System.Drawing.Size(244, 34);
            this.btnRestart.TabIndex = 6;
            this.btnRestart.Text = "Restart";
            this.btnRestart.UseVisualStyleBackColor = true;
            this.btnRestart.Click += new System.EventHandler(this.btnRestart_Click);
            // 
            // lblWinningStreak
            // 
            this.lblWinningStreak.AutoSize = true;
            this.lblWinningStreak.Location = new System.Drawing.Point(189, 13);
            this.lblWinningStreak.Name = "lblWinningStreak";
            this.lblWinningStreak.Size = new System.Drawing.Size(125, 13);
            this.lblWinningStreak.TabIndex = 7;
            this.lblWinningStreak.Text = "Your Winning Streak Is 0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(341, 380);
            this.Controls.Add(this.lblWinningStreak);
            this.Controls.Add(this.btnRestart);
            this.Controls.Add(this.lblLivesLeft);
            this.Controls.Add(this.btnNo5);
            this.Controls.Add(this.btnNo4);
            this.Controls.Add(this.btnNo3);
            this.Controls.Add(this.btnNo2);
            this.Controls.Add(this.btnNo1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnNo1;
        private System.Windows.Forms.Button btnNo2;
        private System.Windows.Forms.Button btnNo3;
        private System.Windows.Forms.Button btnNo4;
        private System.Windows.Forms.Button btnNo5;
        private System.Windows.Forms.Label lblLivesLeft;
        private System.Windows.Forms.Button btnRestart;
        private System.Windows.Forms.Label lblWinningStreak;
    }
}

