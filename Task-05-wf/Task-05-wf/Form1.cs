﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_05_wf
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnNo1_Click(object sender, EventArgs e)
        {
            if (livesLeft == 0)
            {
                MessageBox.Show("You Have No Lives Left. Click \"Restart\" To Play Again");
            }

            else
            {
                string winOrLose = gameProcess(1);
                MessageBox.Show(winOrLose);
                lblLivesLeft.Text = "You Have " + livesLeft.ToString() + " Lives Left";
                lblWinningStreak.Text = "Your Winning Streak Is " + winningStreak.ToString();
            }

        }/**************END OF btnNo1_Click***********/

        private void btnNo2_Click(object sender, EventArgs e)
        {
            if (livesLeft == 0)
            {
                
                MessageBox.Show("You Have No Lives Left. Click \"Restart\" To Play Again");
            }

            
            else
            {
                string winOrLose = gameProcess(2);
                MessageBox.Show(winOrLose);
                lblLivesLeft.Text = "You Have " + livesLeft.ToString() + " Lives Left";
                lblWinningStreak.Text = "Your Winning Streak Is " + winningStreak.ToString();


            }
        }/**************END OF btnNo2_Click***********/

        private void btnNo3_Click(object sender, EventArgs e)
        {
            if (livesLeft == 0)
            {
                MessageBox.Show("You Have No Lives Left. Click \"Restart\" To Play Again");
            }

            else
            {
                string winOrLose = gameProcess(3);
                MessageBox.Show(winOrLose);
                lblLivesLeft.Text = "You Have " + livesLeft.ToString() + " Lives Left";
                lblWinningStreak.Text = "Your Winning Streak Is " + winningStreak.ToString();
            }
        }/**************END OF btnNo3_Click***********/

        private void btnNo4_Click(object sender, EventArgs e)
        {
            if (livesLeft == 0)
            {
                MessageBox.Show("You Have No Lives Left. Click \"Restart\" To Play Again");
            }

            else
            {
                string winOrLose = gameProcess(4);
                MessageBox.Show(winOrLose);
                lblLivesLeft.Text = "You Have " + livesLeft.ToString() + " Lives Left";
                lblWinningStreak.Text = "Your Winning Streak Is " + winningStreak.ToString();
            }
        }/**************END OF btnNo4_Click***********/

        private void btnNo5_Click(object sender, EventArgs e)
        {
            if (livesLeft == 0)
            {
                MessageBox.Show("You Have No Lives Left. Click \"Restart\" To Play Again");
            }

            else
            {
                string winOrLose = gameProcess(5);
                MessageBox.Show(winOrLose);
                lblLivesLeft.Text = "You Have " + livesLeft.ToString() + " Lives Left";
                lblWinningStreak.Text = "Your Winning Streak Is " + winningStreak.ToString();
            }
        }/**************END OF btnNo5_Click***********/

        private void btnRestart_Click(object sender, EventArgs e)
        {
            winningStreak = 0;
            livesLeft = 5;
            lblWinningStreak.Text = "Your Winning Streak Is " + winningStreak.ToString();
            lblLivesLeft.Text = "You Have " + livesLeft.ToString() + " Lives Left";
        }/**************END OF btnRestart_Click***********/



        /*******************************PROGRAM LOGIC***************************/

        static public int livesLeft = 5;
        static public int winningStreak = 0;

        static int RanProcess(int input)
        {
            Random rnd = new Random();
            int ranNumber = rnd.Next(1, 6);
            return ranNumber;
        }/*******************END OF RanProcess******************/

        static string gameProcess(int userInputSend)
        {
            int userInputProcess = userInputSend;
            int ranNumberProcess = RanProcess(userInputProcess);
            string resultReturn = "";


            if (userInputProcess == ranNumberProcess)
            {
                resultReturn = $"You Chose {userInputProcess}  The PC chose {ranNumberProcess}. You Got It Right! \nYou Have {livesLeft} Lives Left\n";
                winningStreak++;
            }
            else if (userInputProcess != ranNumberProcess)
            {
                livesLeft--;
                resultReturn = $"You Chose {userInputProcess}  The PC chose {ranNumberProcess}. You Lose 1 Life \nYou Have {livesLeft} Lives Left\n";

            }

            return resultReturn;

        }/******************END OF gameProcess********************/
    }
}
