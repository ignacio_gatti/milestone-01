﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task_05_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }



        private void btnNo1_Click(object sender, RoutedEventArgs e)
        {
            if (livesLeft == 0)
            {
                
                txtResult.Text = "You Have No Lives Left. Click \"Restart\" To Play Again";
            }

            else
            {
                string winOrLose = gameProcess(1);
                              
                txtLivesLeft.Text = "You Have " + livesLeft.ToString() + " Lives Left";
                txtWinningStreak.Text = "Your Winning Streak Is " + winningStreak.ToString();

                txtResult.Text = winOrLose.ToString();
            }

        }/**********END OF btnNo1_Click***********/

        private void btnNo2_Click(object sender, RoutedEventArgs e)
        {
            if (livesLeft == 0)
            {

                txtResult.Text = "You Have No Lives Left. Click \"Restart\" To Play Again";
            }

            else
            {
                string winOrLose = gameProcess(2);

                txtLivesLeft.Text = "You Have " + livesLeft.ToString() + " Lives Left";
                txtWinningStreak.Text = "Your Winning Streak Is " + winningStreak.ToString();

                txtResult.Text = winOrLose.ToString();
            }
        }/**********END OF btnNo2_Click***********/

        private void btnNo3_Click(object sender, RoutedEventArgs e)
        {
            if (livesLeft == 0)
            {

                txtResult.Text = "You Have No Lives Left. Click \"Restart\" To Play Again";
            }

            else
            {
                string winOrLose = gameProcess(3);

                txtLivesLeft.Text = "You Have " + livesLeft.ToString() + " Lives Left";
                txtWinningStreak.Text = "Your Winning Streak Is " + winningStreak.ToString();

                txtResult.Text = winOrLose.ToString();
            }
        }/**********END OF btnNo3_Click***********/

        private void btnNo4_Click(object sender, RoutedEventArgs e)
        {
            if (livesLeft == 0)
            {

                txtResult.Text = "You Have No Lives Left. Click \"Restart\" To Play Again";
            }

            else
            {
                string winOrLose = gameProcess(4);

                txtLivesLeft.Text = "You Have " + livesLeft.ToString() + " Lives Left";
                txtWinningStreak.Text = "Your Winning Streak Is " + winningStreak.ToString();

                txtResult.Text = winOrLose.ToString();
            }
        }/**********END OF btnNo4_Click***********/

        private void btnNo5_Click(object sender, RoutedEventArgs e)
        {
            if (livesLeft == 0)
            {

                txtResult.Text = "You Have No Lives Left. Click \"Restart\" To Play Again";
            }

            else
            {
                string winOrLose = gameProcess(5);

                txtLivesLeft.Text = "You Have " + livesLeft.ToString() + " Lives Left";
                txtWinningStreak.Text = "Your Winning Streak Is " + winningStreak.ToString();

                txtResult.Text = winOrLose.ToString();
            }
        }/**********END OF btnNo5_Click***********/

        private void btnRestart_Click(object sender, RoutedEventArgs e)
        {
            winningStreak = 0;
            livesLeft = 5;
            txtWinningStreak.Text = "Your Winning Streak Is " + winningStreak.ToString();
            txtLivesLeft.Text = "You Have " + livesLeft.ToString() + " Lives Left";
        }/**********END OF btnRestart_Click***********/



        /*******************************PROGRAM LOGIC***************************/

        static public int livesLeft = 5;
        static public int winningStreak = 0;

        static int RanProcess(int input)
        {
            Random rnd = new Random();
            int ranNumber = rnd.Next(1, 6);
            return ranNumber;
        }/*******************END OF RanProcess******************/

        static string gameProcess(int userInputSend)
        {
            int userInputProcess = userInputSend;
            int ranNumberProcess = RanProcess(userInputProcess);
            string resultReturn = "";


            if (userInputProcess == ranNumberProcess)
            {
                resultReturn = $"You Chose {userInputProcess}  The PC chose {ranNumberProcess}. You Got It Right! \nYou Have {livesLeft} Lives Left\n";
                winningStreak++;
            }
            else if (userInputProcess != ranNumberProcess)
            {
                livesLeft--;
                resultReturn = $"You Chose {userInputProcess}  The PC chose {ranNumberProcess}. You Lose 1 Life \nYou Have {livesLeft} Lives Left\n";

            }

            return resultReturn;

        }/******************END OF gameProcess********************/
    }
}
