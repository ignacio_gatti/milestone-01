﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task_04_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void btnCheck_Click(object sender, RoutedEventArgs e)
        {
            string txtUserInputLowerCase = txtUserInput.Text.ToLower();

            if (FruAndVegProcess(txtUserInputLowerCase) == "\nThis Fruit Is Not In The List\n")
            {
                txtResult.Text = "This Fruit/Vegetable Is Not In The List";
            }

            else
            {
                txtResult.Text = "A(n) " + txtUserInputLowerCase + " is " + FruAndVegProcess(txtUserInputLowerCase);
            }
                
        }

        /*********************PROGRAM LOGIC***************************/

        static string FruAndVegProcess(string userInput)
        {
            string userInputProcess = userInput;
            string FruAndVegReturn = "";
            var fruitAndVeg = new Dictionary<string, string>();

            fruitAndVeg.Add("banana", "yellow");
            fruitAndVeg.Add("apple", "red");
            fruitAndVeg.Add("orange", "orange");
            fruitAndVeg.Add("lettuce", "green");
            fruitAndVeg.Add("tomatoe", "red");
            fruitAndVeg.Add("garlic", "white");
            fruitAndVeg.Add("cherry", "purple");
            fruitAndVeg.Add("carrot", "orange");


            switch (userInputProcess)
            {
                case "banana":
                    FruAndVegReturn = fruitAndVeg["banana"];
                    break;

                case "apple":
                    FruAndVegReturn = fruitAndVeg["apple"];
                    break;

                case "orange":
                    FruAndVegReturn = fruitAndVeg["orange"];
                    break;

                case "lettuce":
                    FruAndVegReturn = fruitAndVeg["lettuce"];
                    break;

                case "tomatoe":
                    FruAndVegReturn = fruitAndVeg["tomatoe"];
                    break;

                case "garlic":
                    FruAndVegReturn = fruitAndVeg["garlic"];
                    break;

                case "cherry":
                    FruAndVegReturn = fruitAndVeg["cherry"];
                    break;

                case "carrot":
                    FruAndVegReturn = fruitAndVeg["carrot"];
                    break;

                case "quit":
                    FruAndVegReturn = "quit";
                    break;

                default:
                    FruAndVegReturn = "\nThis Fruit Is Not In The List\n";
                    break;
            }


            return FruAndVegReturn;
        }/*********************END OF FruAndVegProcess*************************/
    }
}
