﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_01_console
{
    class Program
    {
        static void Main(string[] args)
        {
            string chooseSystem = "";
            double userDistance = 0;
            double conversionResult = 0;
            Error: 
            Console.Clear();
            Console.WriteLine($"Please choose between converting to \"miles\" or \"km\"");
            chooseSystem = Console.ReadLine();
            
            if (chooseSystem == "miles")
            {
                Console.WriteLine($"You chose to convert to \"miles\"\nChoose how many KM to MILES you want to change to\nType \"-0\" if you want to change options");
                userDistance = int.Parse(Console.ReadLine());

                if(userDistance == -0)
                {
                    goto Error;
                }

                conversionResult = convertToMilesProcess(userDistance);
                Console.WriteLine($"{conversionResult} Miles");




                Console.ReadLine();
            }

            else if (chooseSystem == "km")

            {
                Console.WriteLine($"You chose to convert to \"km\"\nChoose how many MILES to KM you want to change to\nType \"-0\" if you want to change options");
                userDistance = int.Parse(Console.ReadLine());

                if (userDistance == -0)
                {
                    goto Error;
                }
                conversionResult = convertToKmProcess(userDistance);
                Console.WriteLine($"{conversionResult} Km");
                Console.ReadLine();
            }

            else 
            {
                Console.Clear();
                Console.WriteLine($"Sorry. You chose an invalid option\nPlease write \"miles\" or \"km\" \nPress Any Key To Continue ");
                Console.ReadLine();
                goto Error;
            }

        }

        static double convertToMilesProcess(double input)
        {
            const double km2Miles = 0.621371192;
            double km = input;
            var answer = km * km2Miles;
            answer = System.Math.Round(answer, 2);
            return answer;
        }

        static double convertToKmProcess(double input)
        {
            const double miles2Km = 1.609344;
            double miles = input;
            var answer = miles * miles2Km;
            answer = System.Math.Round(answer, 2);
            return answer;

        }
    }
}
