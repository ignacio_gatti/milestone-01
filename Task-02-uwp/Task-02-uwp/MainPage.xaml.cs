﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task_02_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            txtResult.Text = $"{shoppingCartProcess(txtUserInput.Text)}";
        }




        //********************************************Program LOGIC**************************************************///
        static public double shoppingCart = 0;

        static double numberProcess(double userInput)/*This is where the user input is processed to include GST in it*/
        {

            double userInputProcess = userInput;
            double GST = 1.15;
            double numberReturnProcess = 0;

            numberReturnProcess = userInputProcess * GST;

            return numberReturnProcess;
        }
        //*****************End of numberProcess*************

        static string shoppingCartProcess(string userInputProcess)/*This is where the items get added together to show the total*/
        {
            double userInput = 0.0;
            userInput = double.Parse(userInputProcess);
            double numberReturn = 0;
            string showShoppingCart = "";

            numberReturn = numberProcess(userInput);

            shoppingCart += numberReturn;

            showShoppingCart = $"You Have ${shoppingCart} in your shopping cart including GST. ";

            return showShoppingCart;
        }//****************End of shoppingCartProcess*******************

        private void btnCheckOut_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Exit();
        }
    }
}
