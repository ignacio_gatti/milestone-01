﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_03_Test_Dummy
{
    class Program
    {

        static void Main(string[] args)
        {

            string optionsReturn = "";
            string questionForUser = $"Which is your {userInput}";
            string secondUserInput = "";
        Restart:
            Console.Clear();
            Console.WriteLine("Lets Talk About Your Favourite \n- Primary Colour\n- Suit of Card\n- Season\n");
            userInput = Console.ReadLine();

            if (userInput == "primary colour" || userInput == "suit of card" || userInput == "season")
            {
                /*nothing*/
            }

            else
            {
                Console.WriteLine("That is not an option. Please Choose A Valid Option");
                Console.ReadLine();
                goto Restart;
            }

            Console.WriteLine($"Which is your favourite {userInput}?");
            secondUserInput = Console.ReadLine();

            optionsReturn = optionsProcess(secondUserInput);

            if (optionsReturn == "That is not an option. Please Choose A Valid Option")
            {
                Console.WriteLine("That is not an option. Please Choose A Valid Option");
                Console.ReadLine();
                goto Restart;
            }


            Console.WriteLine($"{optionsReturn}");
            Console.ReadLine();
        }

        /************************PROGRAM LOGIC**************************************/

        static public string userInput = "";

        static string optionsProcess(string userInputSent)
        {
            string userInputProcess = userInputSent;
            string dictResult = "";
            var responsesPC = new Dictionary<string, string>();

            /*Favourites*/
            responsesPC.Add("Red", $"that is my favourite {userInput} too!");
            responsesPC.Add("Diamond", $"that is my favourite {userInput} too!");
            responsesPC.Add("Summer", $"that is my favourite {userInput} too!");
            /*Least Favourites*/
            responsesPC.Add("Green", $"Really? That is my least favourite {userInput}.");
            responsesPC.Add("Spade", $"Really? That is my least favourite {userInput}.");
            responsesPC.Add("Winter", $"Really? That is my least favourite {userInput}.");
            /*Good*/
            responsesPC.Add("Blue", $"Not my favourite {userInput}, but {userInputProcess} is still really good.");
            responsesPC.Add("Heart", $"Not my favourite {userInput}, but {userInputProcess} is still really good.");
            responsesPC.Add("Club", $"Not my favourite {userInput}, but {userInputProcess} is still really good.");
            responsesPC.Add("Autumn", $"Not my favourite {userInput}, but {userInputProcess} is still really good.");
            responsesPC.Add("Spring", $"Not my favourite {userInput}, but {userInputProcess} is still really good.");

            switch (userInputProcess)
            {
                case "red":
                    dictResult = responsesPC["Red"];
                    break;

                case "diamond":
                    dictResult = responsesPC["Diamond"];
                    break;


                case "summer":
                    dictResult = responsesPC["Summer"];
                    break;

                case "green":
                    dictResult = responsesPC["Green"];
                    break;

                case "spade":
                    dictResult = responsesPC["Spade"];
                    break;

                case "winter":
                    dictResult = responsesPC["Winter"];
                    break;

                case "blue":
                    dictResult = responsesPC["Blue"];
                    break;

                case "club":
                    dictResult = responsesPC["Club"];
                    break;

                case "autumn":
                    dictResult = responsesPC["Autumn"];
                    break;

                case "spring":
                    dictResult = responsesPC["Spring"];
                    break;

                case "heart":
                    dictResult = responsesPC["Heart"];
                    break;

                default:
                    dictResult = "That is not an option. Please Choose A Valid Option";
                    break;
            }

            return dictResult;


        }/*************END OF optionsProcess**************************/


    }
}
