﻿namespace Task_01_wf
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitle = new System.Windows.Forms.Label();
            this.txtInput = new System.Windows.Forms.TextBox();
            this.btnConvert2Miles = new System.Windows.Forms.Button();
            this.btnConvert2Km = new System.Windows.Forms.Button();
            this.lblResult = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Location = new System.Drawing.Point(73, 21);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(130, 13);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "Enter number and convert";
            // 
            // txtInput
            // 
            this.txtInput.Location = new System.Drawing.Point(52, 57);
            this.txtInput.Name = "txtInput";
            this.txtInput.Size = new System.Drawing.Size(177, 20);
            this.txtInput.TabIndex = 1;
            this.txtInput.TextChanged += new System.EventHandler(this.txtInput_TextChanged);
            // 
            // btnConvert2Miles
            // 
            this.btnConvert2Miles.Location = new System.Drawing.Point(35, 128);
            this.btnConvert2Miles.Name = "btnConvert2Miles";
            this.btnConvert2Miles.Size = new System.Drawing.Size(102, 31);
            this.btnConvert2Miles.TabIndex = 2;
            this.btnConvert2Miles.Text = "Convert to Miles";
            this.btnConvert2Miles.UseVisualStyleBackColor = true;
            this.btnConvert2Miles.Click += new System.EventHandler(this.btnConvert2Miles_Click);
            // 
            // btnConvert2Km
            // 
            this.btnConvert2Km.Location = new System.Drawing.Point(143, 128);
            this.btnConvert2Km.Name = "btnConvert2Km";
            this.btnConvert2Km.Size = new System.Drawing.Size(102, 31);
            this.btnConvert2Km.TabIndex = 3;
            this.btnConvert2Km.Text = "Convert to Km";
            this.btnConvert2Km.UseVisualStyleBackColor = true;
            this.btnConvert2Km.Click += new System.EventHandler(this.btnConvert2Km_Click);
            // 
            // lblResult
            // 
            this.lblResult.AutoSize = true;
            this.lblResult.Location = new System.Drawing.Point(49, 201);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(157, 13);
            this.lblResult.TabIndex = 4;
            this.lblResult.Text = "You haven\'t typed a number yet";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.lblResult);
            this.Controls.Add(this.btnConvert2Km);
            this.Controls.Add(this.btnConvert2Miles);
            this.Controls.Add(this.txtInput);
            this.Controls.Add(this.lblTitle);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.TextBox txtInput;
        private System.Windows.Forms.Button btnConvert2Miles;
        private System.Windows.Forms.Button btnConvert2Km;
        private System.Windows.Forms.Label lblResult;
    }
}

