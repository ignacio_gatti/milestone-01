﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_01_wf
{
    public partial class Form1 : Form
    {
        
        
        public Form1()
        {
            InitializeComponent();
             
        }

        static double convertToMilesProcess(string txtInput)
        {
            const double km2Miles = 0.621371192;
            double km = double.Parse(txtInput) ;
            var answer = km * km2Miles;
            answer = System.Math.Round(answer, 2);
            return answer;
        }



        static double convertToKmProcess(string input)
        {
            const double miles2Km = 1.609344;
            double miles = double.Parse(input);
            var answer = miles * miles2Km;
            answer = System.Math.Round(answer, 2);
            return answer;
        }

        private void btnConvert2Miles_Click(object sender, EventArgs e)
        {
            lblResult.Text = $"{convertToMilesProcess(txtInput.Text)}";

        }

        private void txtInput_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void btnConvert2Km_Click(object sender, EventArgs e)
        {
            lblResult.Text = $"{convertToKmProcess(txtInput.Text)}";
        }
    }
}
