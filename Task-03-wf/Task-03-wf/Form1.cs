﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_03_wf
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            
            if (txtFav1.Text == "primary colour" || txtFav1.Text == "suit of card" || txtFav1.Text == "season")
            {
                lbl2ndQ.Text = $"What is Your Favourite {txtFav1.Text}";
            }
            else
            {
                MessageBox.Show("That is not an option. Please Choose A Valid Option");
            }
        }

        private void btnFinal_Click(object sender, EventArgs e)
        {
            if (txtFav1.Text == "")
            {
                MessageBox.Show("Please Fill In Above First");
            }

            else
            {
                userInput = txtFav1.Text;
                MessageBox.Show(optionsProcess(txtFav2.Text)); 
            }

        }/*End of btnFinal*/

        /************************PROGRAM LOGIC**************************************/

        static public string userInput = "";

        static string optionsProcess(string userInputSent)
        {
            string userInputProcess = userInputSent;
            string dictResult = "";
            var responsesPC = new Dictionary<string, string>();

            /*Favourites*/
            responsesPC.Add("Red", $"that is my favourite {userInput} too!");
            responsesPC.Add("Diamond", $"that is my favourite {userInput} too!");
            responsesPC.Add("Summer", $"that is my favourite {userInput} too!");
            /*Least Favourites*/
            responsesPC.Add("Green", $"Really? That is my least favourite {userInput}.");
            responsesPC.Add("Spade", $"Really? That is my least favourite {userInput}.");
            responsesPC.Add("Winter", $"Really? That is my least favourite {userInput}.");
            /*Good*/
            responsesPC.Add("Blue", $"Not my favourite {userInput}, but {userInputProcess} is still really good.");
            responsesPC.Add("Heart", $"Not my favourite {userInput}, but {userInputProcess} is still really good.");
            responsesPC.Add("Club", $"Not my favourite {userInput}, but {userInputProcess} is still really good.");
            responsesPC.Add("Autumn", $"Not my favourite {userInput}, but {userInputProcess} is still really good.");
            responsesPC.Add("Spring", $"Not my favourite {userInput}, but {userInputProcess} is still really good.");

            switch (userInputProcess)
            {
                case "red":
                    dictResult = responsesPC["Red"];
                    break;

                case "diamond":
                    dictResult = responsesPC["Diamond"];
                    break;


                case "summer":
                    dictResult = responsesPC["Summer"];
                    break;

                case "green":
                    dictResult = responsesPC["Green"];
                    break;

                case "spade":
                    dictResult = responsesPC["Spade"];
                    break;

                case "winter":
                    dictResult = responsesPC["Winter"];
                    break;

                case "blue":
                    dictResult = responsesPC["Blue"];
                    break;

                case "club":
                    dictResult = responsesPC["Club"];
                    break;

                case "autumn":
                    dictResult = responsesPC["Autumn"];
                    break;

                case "spring":
                    dictResult = responsesPC["Spring"];
                    break;

                case "heart":
                    dictResult = responsesPC["Heart"];
                    break;

                default:
                    dictResult = "That is not an option. Please Choose A Valid Option";
                    break;
            }

            return dictResult;


        }/*************END OF optionsProcess**************************/

        
    }
}
