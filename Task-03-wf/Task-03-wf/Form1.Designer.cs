﻿namespace Task_03_wf
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl1stQ = new System.Windows.Forms.Label();
            this.lblPriCol = new System.Windows.Forms.Label();
            this.lblSuit = new System.Windows.Forms.Label();
            this.lblSeason = new System.Windows.Forms.Label();
            this.txtFav1 = new System.Windows.Forms.TextBox();
            this.lbl2ndQ = new System.Windows.Forms.Label();
            this.lblResult = new System.Windows.Forms.Label();
            this.txtFav2 = new System.Windows.Forms.TextBox();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.btnFinal = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl1stQ
            // 
            this.lbl1stQ.AutoSize = true;
            this.lbl1stQ.Location = new System.Drawing.Point(93, 33);
            this.lbl1stQ.Name = "lbl1stQ";
            this.lbl1stQ.Size = new System.Drawing.Size(157, 13);
            this.lbl1stQ.TabIndex = 0;
            this.lbl1stQ.Text = "Lets Talk About Your Favourite:";
            // 
            // lblPriCol
            // 
            this.lblPriCol.AutoSize = true;
            this.lblPriCol.Location = new System.Drawing.Point(96, 50);
            this.lblPriCol.Name = "lblPriCol";
            this.lblPriCol.Size = new System.Drawing.Size(74, 13);
            this.lblPriCol.TabIndex = 1;
            this.lblPriCol.Text = "Primary Colour";
            // 
            // lblSuit
            // 
            this.lblSuit.AutoSize = true;
            this.lblSuit.Location = new System.Drawing.Point(96, 63);
            this.lblSuit.Name = "lblSuit";
            this.lblSuit.Size = new System.Drawing.Size(62, 13);
            this.lblSuit.TabIndex = 2;
            this.lblSuit.Text = "Suit of Card";
            // 
            // lblSeason
            // 
            this.lblSeason.AutoSize = true;
            this.lblSeason.Location = new System.Drawing.Point(96, 76);
            this.lblSeason.Name = "lblSeason";
            this.lblSeason.Size = new System.Drawing.Size(43, 13);
            this.lblSeason.TabIndex = 3;
            this.lblSeason.Text = "Season";
            // 
            // txtFav1
            // 
            this.txtFav1.Location = new System.Drawing.Point(99, 102);
            this.txtFav1.Name = "txtFav1";
            this.txtFav1.Size = new System.Drawing.Size(173, 20);
            this.txtFav1.TabIndex = 4;
            // 
            // lbl2ndQ
            // 
            this.lbl2ndQ.AutoSize = true;
            this.lbl2ndQ.Location = new System.Drawing.Point(96, 170);
            this.lbl2ndQ.Name = "lbl2ndQ";
            this.lbl2ndQ.Size = new System.Drawing.Size(116, 13);
            this.lbl2ndQ.TabIndex = 5;
            this.lbl2ndQ.Text = "What is your favourite: ";
            // 
            // lblResult
            // 
            this.lblResult.AutoSize = true;
            this.lblResult.Location = new System.Drawing.Point(96, 282);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(0, 13);
            this.lblResult.TabIndex = 6;
            // 
            // txtFav2
            // 
            this.txtFav2.Location = new System.Drawing.Point(99, 198);
            this.txtFav2.Name = "txtFav2";
            this.txtFav2.Size = new System.Drawing.Size(170, 20);
            this.txtFav2.TabIndex = 7;
            // 
            // btnSubmit
            // 
            this.btnSubmit.Location = new System.Drawing.Point(99, 139);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(170, 23);
            this.btnSubmit.TabIndex = 8;
            this.btnSubmit.Text = "Submit Answer";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // btnFinal
            // 
            this.btnFinal.Location = new System.Drawing.Point(99, 236);
            this.btnFinal.Name = "btnFinal";
            this.btnFinal.Size = new System.Drawing.Size(170, 23);
            this.btnFinal.TabIndex = 9;
            this.btnFinal.Text = "Submit Answer";
            this.btnFinal.UseVisualStyleBackColor = true;
            this.btnFinal.Click += new System.EventHandler(this.btnFinal_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(390, 338);
            this.Controls.Add(this.btnFinal);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.txtFav2);
            this.Controls.Add(this.lblResult);
            this.Controls.Add(this.lbl2ndQ);
            this.Controls.Add(this.txtFav1);
            this.Controls.Add(this.lblSeason);
            this.Controls.Add(this.lblSuit);
            this.Controls.Add(this.lblPriCol);
            this.Controls.Add(this.lbl1stQ);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl1stQ;
        private System.Windows.Forms.Label lblPriCol;
        private System.Windows.Forms.Label lblSuit;
        private System.Windows.Forms.Label lblSeason;
        private System.Windows.Forms.TextBox txtFav1;
        private System.Windows.Forms.Label lbl2ndQ;
        private System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.TextBox txtFav2;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Button btnFinal;
    }
}

